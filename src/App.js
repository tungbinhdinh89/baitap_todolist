import logo from './logo.svg';
import './App.css';
import DemoTheme from './JSS_StyledComponent/Themes/DemoTheme';
import ToDoList from './JSS_StyledComponent/BaiTapStyleComponent/ToDoList/ToDoList';
import LifeCycleReact from './LifeCycleReact/LifeCycleReact';

function App() {
  return (
    <div className="app">
      {/* <DemoJSS />  */}
      {/* <DemoTheme /> */}
      <ToDoList />
      {/* <LifeCycleReact /> */}
    </div>
  );
}

export default App;
