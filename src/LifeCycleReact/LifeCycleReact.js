import React, { Component } from 'react';
import ChildComponent from './ChildComponent';

export default class LifeCycleReact extends Component {
  // constructor phương thức khởi tạo react cung cấp
  // hàm constructor là hàm được react khởi tạo và nó sẽ chạy trước khi giao diện được render
  //phương thức khai báo cũ
  constructor(props) {
    super(props);
    //khi báo theo chuẩn react thì tất cả thuộc tính khai báo trong constructor
    this.state = {
      number: 1,
    };
    console.log('constructor');
  }
  // hàm mounting là hàm tự động kích hoạt dù không được gọi
  // Được gọi khi component này được sử dụng trên DOM (giao diện của app)
  static getDerivedStateFromProps(newProps, currentState) {
    console.log('getDerivedStateFromProps: ');
    return null;
  }

  // Được gọi khi setState hoặc props
  shouldComponentUpdate(newProps, newState) {
    // return true thì chạy tiếp các lifecycle còn lại, ngược lại return false thì sẽ dừng lại không chạy tiếp các lifecycle khác
    return true;
  }

  // render
  render() {
    console.log('render parent');
    return (
      <div>
        <h1>Parent Component</h1>
        <span>Number: {this.state.number}</span>
        <button
          className="btn btn-success"
          onClick={() => {
            this.setState({ number: this.state.number + 1 });
          }}>
          +
        </button>
        {this.state.number > 1 ? <ChildComponent /> : 'render'}
      </div>
    );
  }

  // Được gọi lại sau render và chỉ gọi 1 lần duy nhất (trạng thái mounting)
  componentDidMount() {
    console.log('componentDidMount: ');
  }

  // Lần đầu sẽ không gọi, chỉ gọi khi setState hoặc thay đổi props
  componentDidUpdate(prevProps, prevState) {
    console.log('componentDidUpdate');
  }
}
