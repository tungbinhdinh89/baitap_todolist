import {
  add_task,
  change_theme,
  delete_task,
  done_task,
  edit_task,
  update_task,
} from '../types/ToDoListTypes';

// rxaction
export const addTaskAction = (newTask) => ({
  type: add_task,
  newTask,
});

export const changeThemeAction = (themeID) => ({
  type: change_theme,
  themeID,
});

export const doneTaskAction = (taskID) => ({
  // {return{}} = ({});
  type: done_task,
  taskID,
});
export const deleteTaskAction = (taskID) => ({
  type: delete_task,
  taskID,
});

export const editTaskAction = (task) => ({
  type: edit_task,
  task,
});
export const updateTaskAction = (taskName) => ({
  type: update_task,
  taskName,
});
