import { ToDoListDarkTheme } from '../../JSS_StyledComponent/Themes/ToDoListDarkTheme';
import { ToDoListLightTheme } from '../../JSS_StyledComponent/Themes/ToDoListLightTheme';
import { ToDoListPrimaryTheme } from '../../JSS_StyledComponent/Themes/ToDoListPrimaryTheme';
import { changeThemeAction } from '../actions/ToDoListActions';
import {
  add_task,
  change_theme,
  delete_task,
  done_task,
  edit_task,
  update_task,
} from '../types/ToDoListTypes';
import { arrTheme } from '../../JSS_StyledComponent/Themes/ThemeManager';

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: 1, taskName: 'task 1', done: true },
    { id: 2, taskName: 'task 2', done: false },
    { id: 3, taskName: 'task 3', done: true },
    { id: 4, taskName: 'task 4', done: true },
    { id: 5, taskName: 'task 5', done: false },
  ],
  taskEdit: {
    id: 1,
    taskName: 'task 1',
    done: false,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case add_task: {
      // console.log('todo', action.newTask);
      // kiểm tra rỗng
      if (action.newTask.taskName.trim() === '') {
        alert('Task name is required!');
        return { ...state };
      }
      // kiểm tra tồn tại
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (task) => task.taskName == action.newTask.name
      );
      // tìm thấy !== -1
      if (index !== -1) {
        alert(' Task name already exists!');
        return { ...state };
      }
      // taskListUpdate.push(action.newTask)       giống với      state.taskList = [...taskListUpdate, action.newTask]
      // xử lý xong rồi thì gán tasklist mới vào tasklist hiện tại.
      state.taskList = [...taskListUpdate, action.newTask];
      console.log('newTask: ', action.newTask);
      return { ...state };
    }

    case change_theme: {
      // console.log(action);
      //tìm theme dựa vào action.themeID được chọn
      let theme = arrTheme.find((theme) => theme.id == action.themeID);
      // nếu theme tồn tại thì
      if (theme) {
        // set lại theme cho state.themeToDoList
        //  các kiểu dữ liệu cơ bản như string, number nó sẽ tự động load lại còn riêng đối với kiểu dữ liệu object nó chỉ thay đổi 1 lần duy nhất, nếu muốn thay đổi tiếp thì phải là một object hoàn toàn mới
        state.themeToDoList = { ...theme.theme };
      }
      return { ...state };
    }
    case done_task: {
      // console.log(action);
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => task.id === action.taskID);
      if (index !== -1) {
        taskListUpdate[index].done = true;
      }
      return { ...state, taskList: taskListUpdate };
    }

    case delete_task: {
      // console.log(action);
      let taskListUpdate = [...state.taskList];
      // let index = taskListUpdate.findIndex((task) => task.id === action.taskID);
      // if (index !== -1) {
      //   console.log('index: ', index);
      //   taskListUpdate.splice(index, 1);
      // }
      // filter taskListUpdate để tìm task.id  mà khác với task.id của chính nó để gán lại taskListUpdate
      // gán lại giá trị cho mảng taskListUpdate = chính nó nhưng filter không có task id đó.
      taskListUpdate = taskListUpdate.filter(
        (task) => task.id != action.taskID
      );

      return { ...state, taskList: taskListUpdate };
    }
    case edit_task: {
      // console.log('yes');

      // return { ...state };
      return { ...state, taskEdit: action.task };
    }
    case update_task: {
      // Chỉnh sửa lại taskName của taskEdit
      state.taskEdit = { ...state.taskEdit, taskName: action.taskName };

      // Tìm trong taskList cập nhật lại taskEdit người dùng update
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (task) => task.id === state.taskEdit.id
      );
      if (index !== -1) {
        taskListUpdate[index] = state.taskEdit;
      }
      state.taskList = taskListUpdate;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
