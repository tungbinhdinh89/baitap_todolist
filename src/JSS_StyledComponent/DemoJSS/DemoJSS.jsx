import React, { Component } from 'react';
import { Button, CoolerButton } from '../Components/Button';
import { Navink, StyledNavink } from '../Components/NavLink';
import { TextField } from '../Components/TextField';

export default class DemoJSS extends Component {
  render() {
    return (
      <div>
        <Button className="button-style" mainColor fontSize2x>
          Hello World
        </Button>
        <CoolerButton>Hi Tung!</CoolerButton>

        <StyledNavink id="abc" name="alice" value="1234">
          Tung <p>ahihi</p> <h1>Holle</h1>
        </StyledNavink>
        <TextField inputColor="purple" />
      </div>
    );
  }
}
