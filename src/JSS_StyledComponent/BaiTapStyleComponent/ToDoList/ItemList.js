import {
  Table,
  Tr,
  Td,
  Th,
  Thead,
  Tbody,
} from '../../ComponentsToDoList/Table';
import { Button } from '../../ComponentsToDoList/Button';

// list: { id, taskName, done }[]
function ItemList(list) {
  return (
    <div>
      {list.map((task, index) => (
        <>
          <Tr key={index}>
            <Th style={{ verticalAlign: 'middle' }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button className="ml-1">
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        </>
      ))}
    </div>
  );
}

export default ItemList;
