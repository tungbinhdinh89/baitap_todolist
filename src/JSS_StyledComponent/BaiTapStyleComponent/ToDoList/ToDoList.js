import React, { Component } from 'react';
import { Container } from '../../ComponentsToDoList/Container';
import { ThemeProvider } from 'styled-components';
import { Dropdown } from '../../ComponentsToDoList/Dropdown';
import { Heading2 } from '../../ComponentsToDoList/Heading';
import { TextField } from '../../ComponentsToDoList/TextField';
import { Button } from '../../ComponentsToDoList/Button';
import { Table, Tr, Th, Thead } from '../../ComponentsToDoList/Table';
// import ItemList from './ItemList';
import { connect } from 'react-redux';
import {
  addTaskAction,
  doneTaskAction,
  changeThemeAction,
  deleteTaskAction,
  editTaskAction,
  updateTaskAction,
} from '../../../redux/actions/ToDoListActions';
import { arrTheme } from '../../Themes/ThemeManager';

class ToDoList extends Component {
  state = {
    taskName: '',
    disabled: true,
  };
  // render task to do

  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: 'middle' }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                className="ml-1"
                onClick={() => {
                  // setState là hàm bất đồng bộ nên phải lưu ý khi sử dụng vì để song song bất đồng bộ sẽ xảy ra lỗi rất cao.
                  // this.props thay đổi cũng làm render lại nên nếu setState và this.props để hai hàm bất đồng bộ song song với nhau dễ gây ra lỗi, nên phải setState disabled fasle xong phải dispatch lên redux
                  this.setState({ disabled: false }, () => {
                    this.props.dispatch(editTaskAction(task));
                  });
                }}>
                <i className="fa fa-edit"></i>
              </Button>
              <Button
                className="ml-1"
                onClick={() => {
                  this.props.dispatch(doneTaskAction(task.id));
                }}>
                <i className="fa fa-check"></i>
              </Button>
              <Button
                className="ml-1"
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}>
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  //render task done
  renderTaskCompelte = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: 'middle' }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                className="ml-1"
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}>
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  // viết hàm render theme import ThemeManager
  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return (
        <option key={index} value={theme.id}>
          {theme.name}
        </option>
      );
    });
  };

  // Life Cycle bảng 16 nhận vào props mới được thực thi trước render
  // không khuyến khích sử dụng life cycle này vì đã được đổi tên ở phiên bản mới và sẽ bị xoá trong tương lai
  // componentWillReceiveProps(newProps) {
  //   console.log('this.props: ', this.props);
  //   console.log('newProps: ', newProps);
  //   this.setState({
  //     taskName: newProps.taskEdit.taskName,
  //   });
  // }

  // static getDerivedStateFromProps(newProps, currentState) {
  //   // newProps: laf props mới, props cũ là this.props (không truy xuất được)
  //   // curentState: ứng với state hiện tại this.state

  //   // hoặc trả về state mới (this.state)
  //   let newState = { ...currentState, taskName: newProps.taskEdit.taskName };
  //   return newState;

  //   // trả về null state giữ nguyên
  //   // return null
  // }

  render() {
    // const completedTasks = this.props.taskList.filter((t) => !t.done);
    // console.log('completedTasks: ', completedTasks);

    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          {/* khi người dùng chọn theme sẽ lấy value của theme và tạo ra một action và dispatch lên reducer, reducer sau khi nhận lấy action đó và tìm kiếm trong mảng theme đó ra được một theme trùng với them user đã chọn và gán lại cho themetodoloist ban đầu, sau khi gái trị themetodolist thay đổi giao diện sẽ tự động render lại*/}
          <Dropdown
            onChange={(e) => {
              // lấy value từ e.target
              let { value } = e.target;
              console.log('value: ', value);
              // muốn không bấm submit mà thực hiện hành động thì phải dispatch lúc onchange
              // dispatch value lên reducer
              this.props.dispatch(changeThemeAction(value));
            }}>
            {this.renderTheme()}
          </Dropdown>
          <Heading2>To do list</Heading2>
          {/* <Label>Task name</Label>
        <br />
        <Input /> */}
          <TextField
            value={this.state.taskName}
            onChange={(e) => {
              // handleChange = (e) => {
              //   let [name, value] = e.target.value;
              //   this.setState({ [name]: value });
              // };
              this.setState(
                { taskName: e.target.value }

                //  ,() => {console.log(this.state)};
              );
            }}
            name="taskName"
            label="Task name"
            className="w-50"
          />
          <Button
            className="ml-2"
            onClick={() => {
              // láy thông tin người dùng nhập vào từ input
              let { taskName } = this.state;
              // tạo ra một task object
              let newTask = {
                id: Date.now(),
                taskName: taskName,
                done: false,
              };
              console.log(newTask);
              // đưa task object lên redux thông qua phương thức dispatch
              this.props.dispatch(addTaskAction(newTask));
            }}>
            <i className="fa fa-plus"></i> Add Task
          </Button>
          {/* Nếu disable true thì render button update có disable property còn không thì render ra button update bình thường */}
          {this.state.disabled ? (
            <Button
              disabled
              className="ml-2"
              onClick={() => {
                this.props.dispatch(updateTaskAction(this.state.taskName));
                // dữ liệu người dùng nhập đang chứa trong taskName
              }}>
              <i className="fa fa-upload"></i>
              Update Task
            </Button>
          ) : (
            <Button
              className="ml-2"
              onClick={() => {
                // trước khi setState sẽ lưu trữ giá trị state update vào một biến, sau đó setState taskName = '', lúc update thì đưa giá trị state đã backup vào biến taskName
                let { taskName } = this.state;
                this.setState({ disabled: true, taskName: '' }, () => {
                  this.props.dispatch(updateTaskAction(taskName));
                });

                // dữ liệu người dùng nhập đang chứa trong taskName
              }}>
              <i className="fa fa-upload"></i>
              Update Task
            </Button>
          )}

          <hr />
          <Heading2>Task to do</Heading2>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>
          <Heading2>Task Complete</Heading2>
          <Table>
            <Thead>{this.renderTaskCompelte()}</Thead>
            <Thead>{/* <ItemList list={completedTasks} /> */}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }
  // Đây là lifeCycle trả về props cũ và state cũ của component trước khi render (lifeCycle này chạy sau render)
  componentDidUpdate(prevProps, prevState) {
    // so sánh nếu như props trước đó (task edit trước mà khác task edit hiện tại thì mới set state còn không sẽ rơi vào vòng lặp vô tận)
    if (this.props.taskEdit.id !== prevProps.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(ToDoList);
