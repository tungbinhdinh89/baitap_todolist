import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  padding-right: calc(var(--bs-gutter-x) * 0.5);
  padding-left: calc(var(--bs-gutter-x) * 0.5);
  margin-right: auto;
  margin-left: auto;
  //   color: ${(props) => props.theme.color};
  border: 3px solid ${(props) => props.theme.borderColor};
`;
