import styled from 'styled-components';
export const Button = styled.button`
  color: ${(props) => (props.mainColor ? 'yellow' : 'white')};
  background: linear-gradient(#f74c0b, #ec4736);
  border: none;
  font-size: ${(props) => (props.fontSize2x ? '2rem' : '1rem')};
  border-radius: 0.5rem;
  font-weight: bold;
  padding: 1rem;
  opacity: 1;
  &:hover {
    opacity: 0.7;
    transition: all 0.5s;
  }
  &.button-style {
    // color: yellow;
  }
`;

export const CoolerButton = styled(Button)`
  font-size: 4rem;
  color: green;
`;
