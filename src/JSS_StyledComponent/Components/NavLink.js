import styled from 'styled-components';
import React from 'react';
export const Navink = ({ className, children, ...restProps }) => (
  <a className={className} {...restProps}>
    {children}{' '}
  </a>
);

export const StyledNavink = styled(Navink)`
  color: red;
  font-weight: bold;
`;
